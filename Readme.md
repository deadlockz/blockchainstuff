# Hinweis

## Hinweis

Ich habe noch mal hier und da kleine Hinweise eingefügt, die in den Folien bei
meinem Vortrag am 29.5.2019 fehlten. Diese Hinweise ergaben sich aus der Diskussion und
den Fragen. Alte Folien, die evtl. beim Verständnis helfen könnten, sind mit **Zusatz**
betitelt. Themen und Details, die es nicht geschafft haben, sind im *Anhang* hinter den Quellen. 

## Hinweis 2

22.6. Vorsicht! Bis gestern dachte ich noch, dass eine Tx dadurch verifiziert
wird, indem andere Peers die Output Skripte ihrer Inputs prüfen und nach
doublespendung suchen. Ich dachte, die Peers würden so eine Transaktion
mehrfach signieren und ab z.B. 14 Signierungen sei diese fest. Das ist aber
Quatsch! eine Transaktion ist z.B. 6x Validiert worden, weil es bereits
5 weitere Blöcke in der Chain gibt; also nach dem Block, in dem die
Tx steht.

# Proof of Stake

## Proof of Stake

**Proof of Stake**

## PoW Rückblick

- Krypto Puzzel nette Idee (Stakeholder Verteilung)
- Real-World: Geld/Hardware schafft Ungleichgewicht

## PoS Idee

Du darfst Rätsel leichter machen, wenn du viele Coins hast.

- leichtes Rätsel ersetzt energiefressende ASICs
- Leute mit vielen Coins haben kein Interesse, dass DIESE Währung kaputt geht
- Leute mit "echtem Geld" aber schon

## PoS Vorteile

- Entkopplung Real-World
  - Geld/Hardware Einfluss reduziert
  - kaum CPU/Energie Verschwendung
- Konsens: Coin-Einsatz statt Chainlänge entscheidet

## PoS Nachteile

- Offenlegung der Teilhabe (Stake)
- Reiche werden wieder reicher(?)
  - anders als in Real-World ist Regulation möglich


## PoS Regulation

- einfrieren (Block-Reward nicht sofort nutzbar)
  - kein Schneeball-Effekt
  - kein "Rauskaufen nach Sabotage"
- Coin Age (multiplikativ)
  - statt mit wenig Coins sehr lange ständig probieren ...
  - später kurz probieren


## PoS Blockerzeugung

Peercoin (PoS und PoW)

- ab Tag 31: je Tag Stake Faktor 1-60
- Reward: CoinStake (Bitcoin: CoinBase)
  - Tx an mich
  - Coin Age wird 0

## Peercoin Stake Verteilung

![Peercoin Reichenliste](images/peercoin_richlist.png)






## Proof of Stake Velocity

- Einfrieren förderte/belohnt Fehlverhalten
- Knoten in Peercoin gehen offline

Besser **2. Modell: Alterungskurve (PoS Velocity)**

## Proof of Stake Velocity

Nextcoin

- PoS only
- inital alle Coins erzeugt
- Tx Fee nur Umverteilung
- kurzes einfrieren (1 Tag)
  - effective Balance $B_{e}$

## PoSV Blockerzeugung 

Nextcoin

- Zeitpunkt **letzter Block**
- BaseTarget
  - unter 1 Min: bis zu 7% schwerer als letzter Block
  - sonst: bis zu 12% leichter als letzter Block
- individueller Schweregrad (Target)
  - BaseTarget * $B_{e}$ * secondsSinceLastBlock

## PoSV Nextcoin

![das Rennen um den Nächsten Block](images/nextcoin_gen.png)


## Zusatz

Reddcoin Kontrast zu Nextcoin

- Coin Age statt "Rennen um Block"
- Kurve über 2 Wochen (Peak 1.15)
- Zeichne Kurve an Tafel!

[Link zur Stelle im Github Code](https://github.com/reddcoin-project/reddcoin/blob/773e823dfd74f5d9b3c3f4fbb54d5ba48f778edf/src/kernel.cpp#L73)




## Delegated Proof of Stake

Das **3. Modell**: Delegated Proof of Stake

- Abstimmung, wer nächsten Block machen darf
- Stimmgewichtmodelle möglich
- Aufgaben Orchestrierung
- Bitshares basierte Coins und vermutlich bald Etherium



# Double Spending (PoS)

## Double Spending (PoS)

**Double Spending (PoS)**


## Double Spending (PoS)

- Nichts steht auf dem Spiel ("Nothing at Stake")
- Attacken günstiger als bei PoW
- Verkäufer sieht private Chain zuerst nicht



## Double Spending verhindern

- Chains bilden mit nur wenig "Stake" bleibt unwahrscheinlich
- in "Billigblöcken" Tx nicht signieren
- und/oder wie Bitcoin: via Checkpoints (Client Bootstrap Daten)

## Double Spending verhindern

Peercoin Checkpoints umstritten

- nicht Teil des Protokolls
- Client Sync. mehrfach am Tag
- seit 2018 abschaltbar

[Software Bug: Checkpoint erzeugte 2015 Fork](https://github.com/peercoin/peercoin/issues/100)

## Double Spending verhindern

Nextcoin nutzt als Ersatz zu Checkpoints:

- Client: Tx Reorg > 720 Blöcke (12h) verboten
- Muss Client seine Chain zu weit umorganisieren, muss er sich eine "bessere" besorgen.
- fragwürdig, mit welchen Peers/Teilnetz dieser Client kommuniziert hat

## Zusatz

PoS Nachteile

- Knoten im Netz sind auch hier eher Banken/Aktionäre anstatt Goldgräber
- ICO: initial coin offering (Crowdfunding einer neuen Blockchain)
  - Schon wieder Real-World Spekulation!

# eVoting

## eVoting

**eVoting**

## Zusatz

PoS Blockchain eröffnen neue "Teilhabe"-Modelle

- Soziale Netzwerke: "Liken" als Coin (SocialX)
- Zensur-Resistente "Cloud" (Steemit)
- eVoting ?
  - Raspberry Pi als digitaler Wahlhelfer
- Did we need a Blockchain?
  - Vertrauen in gemeinsames System
  - Zwang es sicher zu machen trotz Öffentlichkeit
  - Einigung unter Partnern (Staat vs. Bürger), die sich ggf. nicht trauen


## eVoting: Möglichkeiten

- billiger & schneller als Papier
- kumulieren und panaschieren (Bremen 3 Tage lang auszählen)
- Mitsprache erhöhen (z.B. Schweiz, Estland)
  - Kanton Zürich: $\varnothing$ 10 Wahlen in 6 Monaten
- Wahlbeteiligung erhöhen
  - EU2019 Estland: 37% Beteiligung, davon 47% eVoting
  - Estland hat Statistik eVoting: Tag, IP/Land, Alter, Geschlecht
  - fragwürdig, ob man solche Statistiken erheben darf
- in Verwendung heisst nicht "sicher"

## eVoting: spezielle Recht und Umsetzung

| Konzept/Problem         | Client-Server       | öff. Blockchain | Papier      |
|:------------------------|--------------------:|----------------:|:------------|
| Wahlrecht               | blind Sign.         | blind Sign.     | ok          |
| geheim                  | IP?!                | Mixnet/Zcash    | ok          |
| glaubhaft abstreitbar   | IP?!                | Mixnet          | ok          |
| öffentlich prüfbar      | meine eigene Abgabe | ja              | begrenzt    |
| ohne Zwang              | re-vote             | re-vote         | Briefwahl?  |
| Hardware/Verantw.       | meins (Client)      | Bürger/Mathe    | Staat       |
| Vertrauen               | ?                   | Mathe           | begrenzt    |
| Wahlgeheimnis/Einfluss  | ISP Angriffe?       | 2 priv. Keys?   | begrenzt    |



## Blind Signierung

**Blind Signierung**: bilde aus public Key (Signierung) eine symmetrische Verschlüsselung, die die Signatur nicht zerstört!


## Zusatz

- Kategorie "Zero-Knowledge"
- Staat soll mein Pseudonym bei der Wahl NICHT kennen
- Wahlsystem soll mich nicht kennen
- trotzdem: Pseudonym muss erkennbar gültig sein
- Konzept: symmetrischer Schlüssel, der Staats-Signatur nicht zerstört
- nutzte Daten des Staats-Public-Keys zur Bildung eines symetrischen Schlüssels


## Zusatz

![grobes Konzept der Blind-Signierung](images/blindsign.png)

## Zusatz

Fordere ich nicht in dem Verfahren das Hashen des Pseudonyms, könnte ich
auch `m'` als Pseudonym mit `s'` als Signatur verwenden. Eine zu naive
Implementierung (RSA) würde dann sogar die Erzeugung weiterer gültiger Signaturen
und Pseudonyme erlauben.



## eVoting: 3 Blockchain Modelle

- Nextcoin (nicht anonym)
- VoteCoin (Zerocash Fork, nicht ausgearbeitet, suspekt)
- GovCoin (UK ewige Debatte)

bisher nichts brauchbares :-(

## eVoting: Liquid Democracy?

- Stimme als Coin (sammlen)
- dynamisch und thematisch/Ministerien verteilen
  - hier wähle ich
  - der hat Ahnung und wählt für mich
  - klassisch: Partei und fertig aus

## eVoting: Chancen?

- Sozial Media Fail?
  - Finanzmittel entziehen
  - Zugangsrechte für Gebäude entziehen
  - Tagesaktuell Stimmrecht/Gewicht ändern
  - Misstrauensvotum (Kurze Kanzlerschaft)
- Präsident Wulff?
  - offiziell ohne Schuld
  - Gewicht Presse/Meinung
- Dystropie "The Orville" [Folge](https://www.serienjunkies.de/orville/1x07-majority-rule.html)
  - der "Masterfeed"
  - up/down Vote statt Rechtssystem
  - "Exekution" bei 10Mio. Down-Votes

## Fragen

**Fragen ?**

## Fragen

- Wieso spart Proof of Stake Energie und Geld?
- Welchen Nachteil hat das Einfrieren der erworbenen Coins bei Peercoin?
- Welche Vorteile hat Delegated Proof of Stake gegenüber "nur" Coin Age?
- Wofür lässt sich Blind-Signierung in einer Wahl verwenden?
- Was versteht man unter glaubhafter Absteitbarkeit?

# Quellen

## Quellen

**Quellen**

[Folienhistorie auf gitlab.com/deadlockz/blockchainstuff](https://gitlab.com/deadlockz/blockchainstuff)

## Quellen

Altcoins

- [Nextcoin Whitepaper](https://nxtwiki.org/wiki/Whitepaper:Nxt)
- [Peercoin](https://docs.peercoin.net/)
- [Peercoin Calculator](http://peercoin.github.io/peercoin-POSCalculator/)
- [Peercoin Reichenliste](https://chainz.cryptoid.info/ppc/#!rich)
- [Bitshares DPoS](https://www.bitshares.org/technology/delegated-proof-of-stake-consensus/)
- [Etherium Wechsel PoS](https://github.com/ethereum/wiki/wiki/Proof-of-Stake-FAQ)

## Quellen

Altcoin Details

- [PDF Bitcoin Überblick](https://eprint.iacr.org/2015/464-pdf)
- [Peercoin Charts und Daten](https://www.peercoinexplorer.net/)
- [PoS Nextcoin Überblick](https://nxtplatform.org/get-started/developers/)
- [Nextcoin Stake Monitor](https://nxtportal.org/monitor/)

## Quellen

E-Voting

- [Guy Fawkes Election](www.jhlee.org/writings/pubs/TD-1998-Election.pdf)
- [Blind Signatures Überblick](www.ijicic.org/ijicic-10-05047.pdf)
- [Mixnet](https://www.researchgate.net/publication/27481358_An_Efficient_Mixnet-Based_Voting_Scheme_Providing_Receipt-Freeness)
- [Wahlsysteme Masterarbeit](https://wwwcn.cs.uni-duesseldorf.de/publications/publications/library/Meter2015a.pdf)
- [VoteCoin](https://votecoin.site/VoteCoin-Whitepaper.pdf)
- [p2p Civitas](https://www.cs.cornell.edu/projects/civitas/)
- [webApp Helios](https://www.usenix.org/legacy/event/sec08/tech/full_papers/adida/adida.pdf)
- [Helios Usability](http://static.usenix.org/events/evtwote11/tech/final_files/Karayumak7-8-11.pdf)

## Quellen

E-Voting Eckdaten

- [Presse Regierung Norwegen](https://www.regjeringen.no/no/dokumenter/Internettvalg/id764303/)
- [Möglichkeiten GovCoin UK](https://de.scribd.com/doc/295987915/Distributed-Ledger-Technology-beyond-block-chain)
- [Artikel Demokratie 2.0](https://bitcoinblog.de/2015/03/09/demokratie-2-0-ermoglicht-die-blockchain-digitale-wahlen/)
- [Liquid Democracy aus Sicht der Piraten Partei](https://wiki.piratenpartei.de/Liquid_Democracy)
- [Swiss Post e-Voting](https://www.evoting-blog.ch/en/pages/2019/public-hacker-test-on-swiss-post-s-e-voting-system)
- [Kanton Bern](https://www.sta.be.ch/sta/de/index/wahlen-abstimmungen/wahlen-abstimmungen/e-voting/haeufige_fragen/rund_um_evoting.html)
- [Kanton Zürich Archiv](https://wahlen-abstimmungen.zh.ch/internet/justiz_inneres/wahlen-abstimmungen/de/abstimmungen/abstimmungsarchiv.html)
- [Estland Statistik iVoting und News](https://www.valimised.ee/en/archive/statistics-about-internet-voting-estonia)

## Quellen

Soz Net / News

- [p2p Diaspora*](https://diasporafoundation.org/)
- [Akasha Etherium Clone](https://beta.akasha.world/#/)
- [SocialX Etherium Clone](https://socialx.network/)
- [Civil Etherium Clone](https://civil.co/)
- [ONZ DPoS](https://www.onzcoin.com)
- [SteemIt DPoS](https://steemit.com/faq.html)



# Anhang

**Anhang**

## Vertrauen in Blockchain

- jede kann mitmachen
- mitmachen bedeutet Belohnung
- Vertrauen in System, weil VIELE kontrollieren
- Vertrauen in 1 Akteur: Nein!
  - Ziele unbekannt
  - Handeln unbekannt

## Vertrauen in Blockchain

Belohnung

- Crypto-Puzzle, um Coins gleichmäßig zu verteilen
- Wer Coins hat: Stakeholder der Währung

## Vertrauen in Blockchain

Leider Monopoly Effekt

- mehr Coins
  - mehr Geld
  - mehr Hardware
  - weniger gleichmäßiges Puzzle

-> Fokus auf 1 Akteur. Vertauen(?)

## PoS Blockerzeugung

Peercoin Konzept

- Mix aus PoW und PoS Blöcken
- Miners und Minters/Forgers (Münzpräger/Schmide)
- 30 Tage einfrieren
- 90 Tage max. Coin Age
- 99% der Coins entstehen aus PoW 
- 50% bis 90% der Blöcke: PoS
  
## PoS Blockerzeugung

Peercoins individueller Schweregrad

    target = 2^224 / difficulty
    dayWeight = min(daysSinceCoins, 90) - 30
    prob = (target * coins * dayWeight) / 2^256

## BITCOIN Difficulty

![BITCOIN](images/bitcoin_diff.png)

## Peercoin Difficulty

![Peercoin Difficulty](images/peercoin_diff.png)

## Peercoin neue Coins

![Peercoin Teilhabe](images/peercoin_powpos.png)

## Peercoin Blöcke

![PoS Blöcke überwiegen](images/peercoin_blockratio.png)

## Peercoin aktive Adressen

![aktive Adressen](images/peercoin_active.png)

## PoS Regulation

Problem Coinage und Einfrieren

- Knoten gehen 90 Tage offline
- Sicherheit schaffen online-Knoten
- belohnt wird das "Falsche" Verhalten


## Allgemein: Variabler Tx Fee?

**Variabler Tx Fee?**

Besser ist ein fester Prozentsatz auf alle Transaktionen im Block, weil sonst
könnten andere sagen: Transaktionen in dem Block signiere ich nicht,
da mir der Block zu teuer ist.


## Billigcoin?

- weniger Hardware-Schlacht
- als Micro-Donate System?
- Mit "Faven" bezahlen?

## Als P2P Technik?

- Alternative sozialen Netzwerke?
- P2P vs. "3GB" Blockchain
  - Anonymität
  - Redundanz
  - wem gehören meine Daten?

## Wer verdient?

- Blockchain as a Service?
  - Ardor Child Chains und Blockchain bloat
  - Kopie einer Chain und Einbau fragwürdiger Funktionen (Did we need a Blockchain)
- Konzepte existent aber trotzdem mit bereits FIAT kaufbar
- Stakeholder?
  - Anteilsbeteiligung bei Farm (PoW)
  - Anteilsbeteilugung an Währung (PoS)




## eVoting in der Praxis

- bisher nur Client/Server Modelle in Betrieb
  - Estland
  - Norwegen 2011/2013 (2014 abgeschafft)
  - Schweiz (z.Z. Einführung umstritten)
- "in Verwendung" heißt nicht automatisch "Sicher"

## eVoting: Recht und Umsetzung

- geheime Wahl
- Wahlgeheimnis: Auszählung erst am Ende
- Wahl & Ergebnis öffentlich prüfbar (?)
- ISP: neue Angriffsvektoren

## eVoting geheime Stimme

- Wahlberechtigt: Blind Signierung (sym. Key, der Signatur nicht zerstört)
- Mixnet (keine Rückverfolgbarkeit)

## E-Voting Nachteile

- Wer kauft Hardware?
- Wer überprüft Hardware/Software?
- finanzielle Interessen/Ziele
- Wer trägt Verantwortung?

## Nötig (de)

- Wahlrecht
- geheim
- ohne Zwang
- keine Wahlmänner (direkt)
- kein Stimmgewicht (gleich)
- fälschungssicher
- (von jedem) überprüfbar
- Wahlgeheimnis

## Weitere Herausforderungen

- glaubhafte Abstreitbarkeit
- Everlasting Privacy

## glaubhafte Abstreitbarkeit

- IT Forensik: Zufällige Daten oder Partition?
- durch Folter, Spyware oder Rechnerklau: Psydonym erkennen
- ich kann dann nicht mehr glaubhaft Versichern, etwas anderes gewählt zu haben

## Everlasting Privacy

- Verschüsselungen auch in Zukunft unknackbar
- Stimmabgabe Mix
  - nur noch die Abgabe (nicht meine Wahl) durch mich erkennbar
  - glaubhaft Abstreitbar
  - Vertraue ich dem Mixnet?
  - Vorsicht! Mixnet ist auch Verschlüsselung!

## Erste Idee

- eine Wahl ist eine Blockchain
- ein Wahlrecht ist ein Coin
- ein Wahlzettel ist eine Transaktion
- eine Wahlurne ist ein Block
- Wahlhelfer sind Forger?

## Offensichtlich kniffliger

Hm. Das passt so nicht:

- Coin darf nich übertragbar sein
- ohne Zwang: umentscheiden möglich
- Wahlgeheimnis: kein vorzeitiges Auszählen!
- Staat kennt meinen Coin


## Mixnet

- nicht einfach nur "Mischen"
- IP Adresse wird irrelevant
- 3 Typen:
  - ähnlich der Blind-Signierung
  - mehrfach Ver- oder Entschlüsselung
  - homomorphe Krytogaphie (z.B. Addition trotz Verschlüsselung möglich)

## Peters eVoting

**Peters eVoting - ein Versuch**

## Peters eVoting

- Blockchain als Zeitstempel
- Blockchain als verteilter Log
- Knoten und Software: öffentlich

## Peters eVoting

- lasse mir mein Pseudonym blind signieren: `P`
- Verwendung von asym. Keys: `pub()` und `priv()`
- Nutzung Hash `h()` und meiner Nonce `N`
- Vote Vektor `v`
  - nur diskrete Kandidaten/Pateinamen

## Peters eVoting Ablauf

- kein Missbrauch von `P` via Commitment: `h(pub(h(vN))P)`
- Meine Wahl: `pub(h(vN))P`
- Wahlende: Veröffentlichung `priv()`
- Zählt mich: Veröffentlichung `N`
- durch Probieren: `v` und `P` finden

## Peters eVoting - Kritik

- ab wann ist Punkt 1 sicher in der Blockchain?
- kann ich nicht in Punkt 1 alle Hashes publizieren? Was hält mich ab?
  - so kann ich in Punkt 2 auch den `P` für einen anderen Vote nutzen
- immernoch kein umentscheiden (soll verhindern, dass man unter Zwang die einzige Stimme abgibt)
- Mixing für glaubhafte Abstreitbarkeit fehlt
- auch in Zukunft kann Staat meine sym. Verschlüsselung nicht knacken?
- Verschlüsselung Performance!!

## Andere Systeme 

**Andere Systeme**

## Estland

- Client/Server
- elektronisch "aufgebohrte" Briefwahl
- doppelter Umschlag $\rightarrow$ 2 public Keys
- ISP hat Schlüsselrolle

## Einwurf: Briefwahl Deutschland

- kann man der Briefwahl trauen?
- Briefwahl nur mit triftigem Grund!
- Wahllokal soll viele nötige Punkte sicherstellen (Zwang?)
- Deutschland Sozialwahl
  - Warum sind da QR-Codes am Rand der Zettel?
  - Wer kontrolliert das öffentlich mit den 2 Umschlägen?
  - Kann ich das mit den QR-Codes selber auf Richtigkeit prüfen?
  - Wenn ich die Codes Abschneide? Gültig? Null-Infos dazu!!


## Norwegen

- Client/Server
- SMS mit Token
- Mixing-Dienst (spanischer Closed Source)
- ISP hat Schlüsselrolle
- 2014 nach 2 Wahlen abgeschafft (Bürger verstanden Sicherheitsmechanismen zu wenig bzgl. geheime Wahl)

## Schweizer Post eVoting

- WebApp/Server
- off. Hacker Test Feb + März 2019
- danach Demoversion offline
- Firmen zur Codeeinsicht fanden Fehler
- diverse 6/26 Kantone wollen es nutzen
- in Schweiz ca. 3 Volksabstimmungstermine im Jahr
- weitere je Kanton!

## Beispiel Kanton Zürich

- Seit 1990: 601 Abstimmungen
- im Schnitt: ca 10 in einem halben Jahr
- immerhin werden Termine zeitgleich gelegt

## Kanton Bern

- WebApp/Server
- seit 2014
- ähnlich wie "Schweizer Post eVoting"
- 4 Wochen vor dem Wahlsonntag (exklusive Wahlsonntag)
- Argument "nicht öffentlich prüfbar" ...

## Kanton Bern

*Die Genfer Kontrollorgane (Vertreterinnen und Vertreter der politischen Parteien) haben Einsicht [...] Zudem kann der Kanton Bern jederzeit zusätzliche Prüfungen des Genfer E-Voting-Systems verlangen. Auf diese Art wird die öffentliche Kontrolle gewährleistet.*

## Civitas

- P2P
- mir unklar: wem gehören die Peers?
- mehrfach Stimmabgabe möglich, wenn ISP zulässt;-)
- JIF unpopuläre Programmiersprache
- keine Referenzen, ob es je zum Einsatz kam
- Mixnet


## Helios

- WebApp/Email/Server
- Mixnet
- für kleine Wahlen (Hochschule) OK
- Teile des Wahlsystems/Ablaufs könnten normale Wähler verunsichern/überfordern


## GovCoin

GovCoin wurde u.a. diskutiert als alternative Währung für die britischer Wohlfahrt

- Miete
- Wasser
- Essen
- ...

Umfang von GovCoin sprengt den Rahmen: [Mitschrift UK Parlamentsdebatte](https://hansard.parliament.uk/Lords/2017-03-27/debates/D34EDCAF-56D6-46E7-A16F-C4DB2FCA9890/Govcoin)

## VoteCoin

- Blockchain
- nutzt Zerocash Protokoll (mixing/zero-knowledge ähnlich Blind Signierung)
- kam noch nicht zum Einsatz
- Whitepaper ist eher Werbebroschüre (wie so oft)
- (noch) keine Details bzgl. Wahlvorgang
- Initial Coin Offer! (es wird wieder Geld verdient)

## Liquid Democracy Chancen?

Stimmgeld zur eigenen politischen Einflussnahme oder als Vertrauenspunkte
für andere Akteure. Kauf möglich?

