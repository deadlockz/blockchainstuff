Vorsicht. Das sind meine eigenen Notizen. Diese könnten unbefriedigend knapp und auch falsch sein!

- [Zusammenfassung zur "Blockchain" Technik und dessen Nutzen](#zusammenfassung-zur-blockchain-technik-und-dessen-nutzen)
- [00 Grundlagen: P2P](#00-grundlagen-p2p)
- [0 Grundlagen: Hashfunktion](#0-grundlagen-hashfunktion)
- [0 Grundlagen: Blockchain](#0-grundlagen-blockchain)
- [1 Konsensfindung](#1-konsensfindung)
- [2 Mining und PoW](#2-mining-und-pow)
- [3 Transaktionen](#3-transaktionen)
- [4 Netzwerk und Protokoll](#4-netzwerk-und-protokoll)
- [5 Wallets](#5-wallets)
- [6 Mining](#6-mining)
- [7 Anonymität und Identifikation (Kapitel 6)](#7-anonymit%C3%A4t-und-identifikation-kapitel-6)
- [8 Alternative Mining Puzzels](#8-alternative-mining-puzzels)
- [9 Proof of Stake und eVoting](#9-proof-of-stake-und-evoting)
- [10 Overlays](#10-overlays)
- [11 Mining Pools](#11-mining-pools)
- [12 Arbitrary Content](#12-arbitrary-content)
- [13 Altcoins](#13-altcoins)
- [14 Etherium](#14-etherium)
- [15 Braucht man eine Blockchain](#15-braucht-man-eine-blockchain)


# Zusammenfassung zur "Blockchain" Technik und dessen Nutzen

Auch wenn beim Begriff "Blockchain" immer "Cryptowährung" mitklingt,
so ist die Blockchain Technik mehr als nur ein verteiltes Logbuch,
wo Leuten mit einem Public Key ein Recht auf eine Änderung eines
Kontostands (durch signierung einer Transaktion) ermöglicht wird:

- es ist ein P2P Netzwerk
- man geht im Netzwerk von Teilnehmern aus, die sich weder kennen noch vertrauen
- es ist eine Datenstruktur, mit dem Charakter einer verketteten Liste,
  welche in der Regel bei jedem Teilnehmer komplett als Kopie gespeichert ist
- durch asymmetrische Cryptographie werden Änderungsberechtigungen
  eingeschränkt oder erweitert
- man einigt sich auf einen neuen Zustand der Liste gegenüber eines alten Zustands
- mit hilfe von Hash-Werten wird gewährleistet, dass Änderungen, auf die
  man sich zuvor geeinigt hat, sofort auffallen

Die letzten 2 Punkte sind die wichtigsten, da diese sich gegenüber von
Datenbanken massiv unterscheiden: Die Teilnehmer des Netzwerks
müssen den Teilnehmern mit Schreibrechten (die, die Blöcke erzeugen)
nicht vertrauen. Durch die Konsensfindung wie beim Generäle-Problem
und der Überprüfbarkeit des zuvor entstandenen und jetzt neu
entstehenden Zustands, werden Fehler herausgefiltert. Man vertraut
darauf, dass die Teilnehmer des Netzwerks sich in vorhersehbarer
Weise auf zuvor abgestimmten gültigen Regeln verhält.

Gegenüber anderen P2P Speichersytemen bietet die Blockchain eine
gewisse Garantie, dass sich Daten darin befinden (inklusive eines
relativen Zeitstempels) und schafft ein P2P System, was einem
gut gepflegten verteilten System nahe kommt.

Viele Anwendungen, für die eine Blockchain missbraucht wird, sind
auch mit einem Server/Cluster als Datenbank realisierbar.
Allerdings setzen diese Anwendung in der Regel eine komplexe
Administration von Schreib- und Leserechten vorraus, welche
nur sehr träge bei neuen Teilnehmern reagiert. Die aktuellen
Blockchains, die es so gibt, haben eine simple Skript Logik, mit der
sich diese Berechtigungen via Account, Hash-Werten, public und
private Keys sowie Signierungen abbilden lassen. Zugriffs-Hierarchien
lassen sich so dezentral realisieren und von allen Teilnehmern des
Netzwerks auf Korrektheit überprüfen.

Thema **Energieverschwendung**: Je nach nutzen einer Technik, ist die
Energie nicht verschwendet. Schaut man sich alternative Blockchains bzw.
Altcoins an, so wird hier nicht zwingend ein aufwändiges Crypto-Puzzel
gelöst, damit der, der es lößt ein Schreibrecht für die Chain bekommt (Blockerzeugung),
sondern auch andere Modelle wurden probiert:

- Peercoin mit Proof of Stake: Simulation einer tollen Hardware, wenn man
  nachweisen kann, dass man einen großen Teil der "Coins" dieser Währung besitzt
- Primecoin: Das Ergebnis des Crypto-Puzzels ist eine Zahlenfolge, die
  in der Mathematik und Cryptographie von nutzen ist

Die Suche nach energiesparenden (und somit geldsparenden) Modellen ist noch
nicht abgeschlossen. Dies gilt auch für die Suche nach Möglichkeiten, wie sich
die Rechenpower und die Speicherresourcen eines solchen Netzwerks nebenher auch
für andere Zwecke nutzen lassen (z.B. Nachweis, dass man bestimmte Daten
gespeichert hat, statt des Nachweises ein Crypro-Puzzel gelöst zu haben). 

Thema **Konsensfindung**: Die Proof-of-Work Konsensfindung ist mit
Raft vergleichbar. Erstmal muss ein Knoten herausfinden, wer aktuell
der Leader Knoten ist. Hierzu muss er andere Knoten nach den letzten
Blöcken fragen. Möglicherweise bekommt er hier unterschiedliche
Ergebnisse genannt, aber in Bitcoin ist wichtig:

- die längste valide Block-Kette
- und natürlich das Mehrheitsprinzip: Warum sollte ein neuer Knoten
  eine Block-Kette übernehmen, die von den meisten anderen Knoten
  nicht akzeptiert ist?

Hat der neue Knoten nun den aktuellsten Block gefunden und sammelt
fleißig Transaktionen (Tx), kann der einen Block bilden und versuchen, dessen
Kryptorätsel/Puzzel (= das "Work" in "Proof of Work") zu lösen. Nebenbei
sollte er die Tx auf Double Spending prüfen. Sollte der neue Knoten
das Rätsel vor den anderen Knoten lösen, darf er sich selbst als Ledger
erklären und den Block im Netzwerk verbreiten. Das Kryptorätsel bestimmt
also die zufällige Zeit, wann ein Knoten Leader sein darf - dessen Berechtigung
ist für alle am erfolgreichen Lösen des Rätsels erkennbar.



# 00 Grundlagen: P2P

## Merkmale P2P Netzwerke

- dezentral, ggf. dynamische Super-Peer-Wahl
- gleichberechtigte Peers (Server und Client)
- unterschiedliche (CPU/Speicher) Ressourcen und Bandbrteiten
- "Hardcore" Verteiltes System
- Churn: stark variable Teilnahme am Netzwerk
- Böse Peers müssen beachtet werden

## P2P Overlays

- Aufbau/Routing eines Netzwerks auf Anwendungsebene
- keine neue Pererherie/Stecker
- Idee z.B. des Content- oder Anwendungsspezifischen Routings
- Oft eigenes Konzept für Adressierung statt nur IP-Adresse

## P2P Arten

Strukturiert:

- Daten und Peers teilen sich in der Regel einen Adressraum
- speichern aufwändig, suchen relativ einfach

Unstrukturiert:

- man kennt nur Peers durch mithöhren oder andere formen 
  der Publikation von IP-Adressen
- speichern einfach, suchen langsam


## P2P Flooding

- Breitensuche
- Anfrage an alle (Gossip: ein paar zufällige)
- Da Netz und nicht Baum: gefahr durch schleifen
- TTL: 6 reicht oft

## P2P Atacken

- Problem Balance zwischen "Nutzung" und "Resourcen zur Verfügung stellen"
- Sybil: mit Fake-Accounts einen Besitz vieler Knoten vortäuschen
- Popularität eines Netzwerks angreifen durch Fehlinfos







# 0 Grundlagen: Hashfunktion

## Eigenschaften kryptografische Hashfunktion

- soll Fingerabruck zu Daten liefern
- Eingaberaum: UNendlich
- Bildbereich: endlich!
- Hashkollisionen gibt es, aber: Kollisionsresistent!
  - Es soll unmöglich sein eine 2. Eingabe zu selben Hashwert zu finden
- Hiding: mit einer Nonce wird ein Schließen auf die (ggf. endliche Eingabe)
  der hash-Funktion unmöglich
- Puzzle Friendliness
  - r element 2 hoch n
  - hash(r x) = y
  - nicht möglich mit weniger als 2 hoch n versuchen ein x zu finden
    bei gegebenen r und y


## Was ist eine Commitment

- schmalgewichtige Signatur
- hash einer Info verbirgt dessen Info
- nachweis, dass man etwas zu einem bestimmten Zeitpunkt bereits wusste




# 0 Grundlagen: Blockchain

## Was ist eine Blockchain

- eine verkettete Liste, wo der Pointer jedoch ein Hashwert des vorherigen Eintrags ist
- zu der Technik wird oft auch eine Konsensfindung unter vielen berechtigten Writern und
  einer Überprüfbarkeit ihres Inhalts und formalen Aufbaus verstanden
- unveränderbares Konto-Buch (=distributed ledger)
- Sicherheitsverpacktes Logbuch
- Block:
  - id (i.d.R. aufsteigend, ggf. Speicheradresse)
  - Daten (z.B. merkle tree)
  - hash des Inhalts des vorherigen Blocks (nicht nur dessen Daten-Partition)

## Wodurch entsteht die Eigenschaft der Manipulationserkennung

- kryptografische hashpointer auf vorgänger Block garantiert Verifizierung des Vorgängers
- merkle tree: (sicherer) Top Hash schafft Möglichkeit, Teilhashes von
  Teildaten aus potentiel unsicheren Quellen auf manipulation prüfen zu können
- man muss zur neubildung der Chain und dessen geänderten Daten die Cryptopuzzel alle neu berechnen. Das
  ist sehr zeitaufwändig und in der zwischenzeit ist die Chain bereits länger geworden
  und die neu berechnete Chain (der Abschnitt) ist dann nicht mehr in die
  neue Chain einfügbar (es sei denn, man habe keine Kollisionsresistenz).

## Ist git eine Blockchain?

Ja, wenn klar ist, dass git den alten header/hash beim neuen Commit mit hasht.


# 1 Konsensfindung

Allgemein ist das hier ein gutes [Übersichtspaper zur Konsensfindung speziell in Blockchains](https://arxiv.org/abs/1711.03936).

## Vor- und Nachteile eines dezentralen Netzes

pro

- Teilhabe an einer Idee/Technik
- hohe Ausfallsicherheit
- gute Lastverteilung möglich

contra

- Durchsatz und Latenz Probleme: Konsistenz nur in Vergangenheit erreicht
- hohe Komplexität
- Loch in Firewalls und ISP-NAT
  (nicht jeder "Client" kann wie ein Server agieren)

## Raft

- in Cluster 1 Leader
- election: random timeout (erforderte Gleichzeitigkeit), ab dem ein Knoten sich selbst wählt und dies
  anderen mitteilt (es sei denn, er bekommt zuvor mitgeteilt, dass sich
  wer anderes al leader wählte
- leader entscheidet, was von einem client in das log aufgenommen wird
- follower replizieren log

## Zwei Eigenschaften, damit verteiltes Konsens-Protokoll funktional ist

Situation: unter vielen Knoten können wenige falsche Werte liefern

- Konsens Algorithmus terminiert unter ehrlichen Knoten mit einem
  Wert als Ergebnis
- der Wert wird von einem der ehrlichen Knoten gebildet

## Konsens Bitcoin

- PoW: Cryptorätsel entscheidet über Leader (andere können prüfen, ob er berechtigt ist, einen Block zu bilden)
- Tx Broadcast dauert ggf. 10min
- Tx wird nicht zwingend in Blockchain aufgenommen
- Tx erst nach ca 6 nachfolge Blöcken (hohe Zustimmung) sicher
- Konsensprozess dauert daher eine Weile

alternativ:

- Proof-of-Work: Puzzel gibt wie bei Raft das zufällige Zeitfenster vor
- Puzzel (auf basis eines validen Parent Blocks) zeigt anderen anständigen
  Peers, dass man bereichtigt ist (und auch anständig ist)
- terminiert, wenn man genug Hashingpower im Netzwerk hat

Oh! Haben plötzlich alle kein riesen Hashingpower, kommt es nicht mehr
zu neuen Blocks und zur neuverhandlung der Target-Difficult

## Was ist nötig, damit ein double spend erfolgreich sein kann

- Fork bzw. vor Leistungsgeber unsichtbare Chain
- Leistunggeber geht von hinreichend verifizierter Tx aus
- eine 2. Tx an (mich) ist in unsichtbarer Chain
- unsichtbare Chain wird sichtbar und erlangt größte länge
- Leistungsgeber hat Leistung für mich erbracht aber erhält value nicht


Man braucht einen ungeduldiger Mensch und eine Transaktion in je einem
Fork von Zweien in der Blockchain. In beiden Tx wird der selbe Output
genutzt, aber in einem für den Geschädigten nicht-öffentlichen Fork
geht der Output woanders hin (an mich?). Sollte dieser Fork öffentlich
werden und länger sein, als der Fork, wo der Geschädigte dachte, dieser
sei valide, greift die Tx an mich und die andere Tx fiegt mit sammt der
Blöcke des Forks aus der Blockchain.


# 2 Mining und PoW

## Warum lohnt es sich mehr ehrlich als unehrlich zu sein

- man schadet der eigenen Währung, die man besitzt
- da der größte Teil ehrlich ist, tritt man gegen einen großen Gegner an
- unehrliche Angriffe sind mit Strom/Hardware Kosten verbunden

## Wie wird das Geld in Umlauf gebracht

- Fee und/oder Blockreward


## Warum wird die Target Difficulty immer wieder angepasst?

Ziel ist, dass ca alle 10 Min ein Block gefunden wird. Dafür wird die
Hashingpower und Transaktionsmenge ca. alle 2 Wochen (2016 Blöcke) analysiert, was
eine akzeptierbare Target difficulty ergibt.

## Wie wird der nächste Block gefunden?

- krypto Puzzel via Nonce und Content in Basecoin
- Konsens über längste Kette gültiger Blöcke

## Wie/Ist es möglich die Transaktionen eines bestimmten Teilnehmers zu verhindern?

- versuch, dass diese Tx nicht weitergeletet wird
- diebstahl seiner Keys und viele Tx mit selben Input broadcasten
- 51% der knoten besitzen und diese Tx nicht signieren (bzw. Block, in den diese ist)
- Chain bilden, die länger als bisherige Chain ist, aber in der man diese Tx
  nicht aufgenommen hat
- feather forking:
  - pool mit 20% hat 4% wkeit, 2 Blöcke zu bilden
  - abfangen TX und ggf. Blöcke anderer Pools: dann bilden des Blocks ohne gewünschte Tx


Sollte man immer den neuen nächsten Block bilden können, dann kann man auch
die Tx immer weglassen. Die Wkeit, dass man über einen sehr langen Zeitraum
dies kann, ist gering. Mit feather forking wäre es möglich, dass man im Schnitt
einen Teil der Transaktionen verhindert. Dies würde irgendwann andere im Netz
dazu bewegen, ebenfalls die Transaktionen weg zu lassen, da sie mit einer
gewissen wkeit von dem bösewicht mit einem fork überholt werden. Ein Mining
solcher Blöcke auf Basis von Blöcken mit dieser Tx darin würde unnötig
ressourcen verschwenden.

Zusätzlich könnte man im Netzwerk auf IP/Routerebene versuchen, den Teilnehmer
und dessen absenden der Transaktionen verhindern.



# 3 Transaktionen

## Was ist eine Escrow Transaction

Escrow = Treuhand

es finden 2 transaktionen statt:

- zuerst wird eine Transaktion mit einem output erzeugt.
- das script der transaktion sieht 2 von 3 signierungen vor, um
  über den neuen value in einer neuen Transaktion verfügen zu dürfen
- der Treuhändler oder die beiden anderen brauchen also von mind. einem
  weiteren Akteur eine signierung der Transaktion

komplexe scripte!

## Beschreibe wie ein Script abgearbeitet wird

- ohne schleifchen
- von links nach rechts
- bytes, die kein OP Code (bzw. längenangaben für deren Parameter) sind, landen im Stack
- Tx Valid: top stack item is not 0 = true, und kein vorzeitiger scriptabbruch

## Was ist Pay-to-hash

Unklar: Pay to skript hash oder pay to pubkey hash?

Der standard ist pay-to-pubkey-hash. Zum einen muss ich in einer neuen
Transaktion zeigen, dass ich mit einer signatur der neuen transaktion im besitz
eines privatekeys bin, welcher zu meinem public key passt. das ist jeweils der
scriptSig eintrag der inputs der neuen Transaktion. diese prüfung passiert zu
letzt im script. zusätzlich muss mein public key gehasht dem hashwert
entsprechen, welcher im output skript der alten transaktion enthalten ist
(ScriptPubKey). Das Outputscript definiert also die Verwendung in einer oder
mehrerer(???) zukünftiger Transaktionen.





# 4 Netzwerk und Protokoll

## Unterschied Hard-Fork und Soft-Fork im Bezug auf Änderungen des Bitcoin Protocols

Hard-Fork

- will man nicht
- protokoll erweiterung, die von anderen (alten) knoten als invalide angesehen wird

Soft-Fork

- langsame verbreitung neuer funktionen
- eine verschärfung des Protokolls bzw. eine abwährtskomp.
- besser, weil sich nicht netzwerk schlagartig verkleinert (gefahr double spending)

## Routing Protokoll: Wie werden Transaktionen und Blöcke im Netzwerk verbreitet

Gossip

- leichtgewichtiger broadcast/flooding
- zufällig an einen/wenige peers weiterleiten
- passiv: "hast du was neues für mich?"
- aktiv: "weisst du schon das neuste?"
- TTL und nicht weiterleiten, wenn man es schon kennt

## Wie geht Beitritt und Verlassen beim Bitcoin Netzwerk

Beitritt:

- neuer peer braucht seed node (z.B. IP adressen durchprobieren)
- seed node gibt weitere peers
- neuer peer sammelt von denen auch deren peers
- neuer peer entscheidet, mit welchen (viele) er sich verbindet und ist so beigetreten

Verlassen: einfach so, andere peers vergessen einen mit der zeit

## Die vier Merkmale der Coinbase Transaktion

- input hash ist 0000000...
- coinbase dummy parameter
- bitcoin: nur 1 Input und 1 Output
- out ist der miner reward (höhe muss/wurde Consens gefunden, reduziert sich immer wieder etwas)

## Komponenten des Bitcoin Blocks

- vorheriger block hash
- nonce
- top/root hash des merkle tree
- Transaktionen + coinbase


# 5 Wallets

Wallet adresse = base58(00 byte concat Ripemd-160 hash(pubkey))

wie base64, aber ohne 0OIl plus und slash um verwechselungen zu vermeiden

Wallets speichern den Private Key!

## Was sind und wie bekommt man vanity addresses

hash mit lesbarem anteil. man muss lange unterschiedliche pubkeys durchprobieren.

## nenne 4 Wallet-Arten: Vor- und Nachteile


- hardware: sehr sicher, wenn man hersteller vertraut. USB stick mit zum Teil generator-funktion
- brain: komfortabler Passwortschutz muss gut sein, aber zu knapp ist cracking möglich.
- paper: sehr unsicher (Video überwachung), gut bei Handy, aufladen, gewinnspiel, werbung(?)
- online
  - Verfügbakeit, Komfort, 3. hat möglicherweise meinen priv.key oder browser/webseite ist unsicher

## Ballance der 3 Ziele der Wallet-Arten

- Verfügbarkeit (nicht usb stick im tresor einer bank)
- Sicherheit (Diebstahl/Verlust)
- Bedienkomfort

## Beispiele und Unterschiede Hot-Storage und Cold-Storage

Cold (USB Stick)

- offline und zugriffgeschützt
- alle coins dauf
- Adresse generation, damit niemand weiss, wo das viele Geld liegt
- hierarchical Adressen:
  - kann mir via index neue keys/Adressen bilden zu verbergung des coldstarage

Hot (Handy, PC, Online,...)

- immer zugreifbar
- nicht alles drauf
- Laptop/Smartphone
  - immer dabei
  - komfortable
  - Sicherheit: man sollte mit einem Wallet mit wenig geld arbeiten, damit
    nicht gleich alles weg ist


## Secret Sharing

Sie sind ein erfolgreicher Bitcoin Dieb und wollen die Geheimzahl 
rekonstruieren, die zu einer Adresse mit 42 Bitcoin gehört. Sie wissen das die Geheimzahl 
sehr schwach verschlüsselt wurde und Sie nur 2 Teile zur Rekonstruktion benötigen. 

Durch Zufall fallen Ihnen folgende 2 Teile in die Hände

- Number=2, Value=100
- Number=6, Value=200 

Rekonstruieren sie die ursprüngliche Geheimzahl.

Antwort:

- 2 Punkte: Gradengleichung
- nummer-steigung: 4 auf value differenz 100
- Value wäre bei Wert 0 mit nummer -2
- Value 42 wäre bei Nummer = -2 + 4*value/100 = -2 + 4*42/100
  ist -2 + 1,68 = -0,32
- unsinn: das geheimnis IST -2 !




# 6 Mining

## Wofür dient das target / die difficulty?

anpassung des schweregrads des miningpuzzels (menge an vorangestellten nullen beim hash ergebnis)

## Auf welcher Hardware lässt sich minen, nennen Sie einige Vor- und Nachteile?

- CPU (hat jeder, aber zu langsam)
- GPU (konsumer markt, aber nicht optimal für integer und hashing operationen, Hitze!)
- FPGA (anschaffung teuer, energie teuer)
- ASIC (Bereits veraltet wenn geliefert wurde. Nur wenige Hersteller. Anschaffung so teuer dass sich Betrieb gerade so lohnt, Hitze!)

## Welche Arten von Energie werden beim Minen benötigt?

- Bei der Herstellung
- Energieverbrauch
- Kühlung




# 7 Anonymität und Identifikation (Kapitel 6)

## Bitcoin-Netzwerk: pseudonym oder anonym

- Anonym: Namenlos, Unbekannt, Verdeckt
- Pseudonym: erfundener Name zur Verdeckung der Identität
- Bitcoin-Wallet Adresse: pseudonym

In Bitcoin kann jeder mehrere Psyeudonyme benutzen, was
zur Identitätsverschleierung dient. Anonymität würde es geben,
wenn man nicht mal den Hinweis eines Zahungsempfängers oder Senders hätte.

Wichtig ist Unlinkabilität:

- Adresspseudonyme sollen nicht mit eineander Verknüpfbar sein
- Tx sollen nicht miteinander verbunden werden können (der bekam das Geld zuvor durch ...)
- Zahlungs-Sender/Empfänger sollten nicht eindeutig zu bestimmen sein

## Einzahlungen: Warum besser immer eine neue Bitcoin-Adresse

So bekommt man nicht im Netzwerk mit, dass hinter dieser einenen Adresse unmengen an Coins liegen.
Sonst weiss man, in welches Wallet ein einbruch lohnt.

Außerdem: die zusammenführung 2er Adressen in eine out-Wechselgeldadresse
ist so identifizierbar. Mit jeweils 2 oder mehr neuen out adressen ist nicht
mehr erkennbar, wie viel man bezahlt hat und was genau das wechselgeld ist.

## Mixing-Dienst: Warum zeitverzögert

"der erste Peer, der dich über eine Transaktion informiert, ist wahrscheinlich der Zahlungssender"
und damit hab ich dessen IP-Adresse. Ein Delay würde hier abhilfe schaffen.

Nein. Bessere Anwort: Geldwäsche/Mixing-Dienst braucht genug Teilnehmer (mit selben hohen Einzahlung)
um gut in und out mischen zu können.

Mixing-Dienst: Man muss 3. vertrauen.

## Warum ist beim CoinJoin kein Coin-Diebstahl möglich?

Es ist nur eine Transaktion mit mehreren In und Outs und muss von allen Teilnehmern signiert werden.



## Warum macht es bei CoinJoin Sinn mehr Output-Adressen als Input-Adressen anzugeben?

Man verhindert, dass so der Input von 2 zuvor unabhängige Adressen nun nachweislich zusammengeführt werden.
Nein, besser: das mix stärker und teilnehmer von CoinJoin können ebenfalls nicht
mehr gut identifizieren, wer was signiert hat.

Offen: IP-Adressen der Teilnehmer




# 8 Alternative Mining Puzzels

## Nenne 3 Anforderungen für ein Puzzel, um eine fair/stable blockchain zu gewährleisten 

- schneller korrektheit verifizierbar als dessen suche
- schweregrad muss anpassbar sein
- je mehr hashingpower ich habe, desto wahrscheinlicher ist das lösen des puzzels
  - außerdem: ein zwischenspeichern alter (falscher) ergebnisse um schneller zu werden, das soll nicht sein
  - nennt sich: *Progress-Freeness*
  - die anzahl der nötigen Schritte um Lösung des Puzzels zu finden, sollte nicht fix sein

## Was ist ein memory hard puzzel und warum wollen wir das?

- ein großer speicherbereich mit annähernd zufälligem zugriffsort ist zur lösung nötig
- ASIC resistenz gewünscht für beteiligung normaler PC User
- memory zugriffszeiten haben ein limit, was nur langsam (gegenüber CPU power) wächst

## Beschreibe grob das Proof-of-useful-Work Konzept

- soll das mining puzzel ablösen
- das ergebnis des "puzzels" soll eine sinnvolle real-world verwendung haben
- könnte z.b. erzwingen von speicherung einer großen datenmenge
- Cunningham Ketten
- instanzen der Industie
- probleme, ob für alle das nützlich ist





# 9 Proof of Stake und eVoting

## Wieso spart Proof of Stake Energie und Geld?

Mit dem Nachweis von Stake darf man des Rätsel (Target) leichter machen.
Auf diese Weise muss man nicht mehr echtes Geld in Hardware stecken, um eine
höhere Wkeit beim Mining/Minting zu haben. Energiefressende Hardware ist also
nicht mehr notwendig. (PS: betrieb, kühlung und Herstellung verbraucht ebenfalls energie)
Ferner wird durch das "Altern" der Coins ein Mechanismus
geschaffen, so dass man statt lange-ständig mining genauso gut später-kurz minting
machen kann. 

## Welchen Nachteil hat das Einfrieren der erworbenen Coins bei Peercoin?

Die Peers gehen offline und warten ab, bis das Coinage ein Maximum erreicht hat.
Da aber das Netzwerk von Peers lebt (verifizierung von Transaktionen durch aufnahme 
von Blöcken in die lokale chain) die
online sind, wird das Netzwerk geschwächt (Viele-Augen-Prinzip nicht gewährleistet).

## Welche Vorteile hat Delegated Proof of Stake gegenüber "nur" Coin Age?

Bei DPoS können Aufgaben im P2P Netz orchestriert werden (wer muss den nächsten block machen)
und unterschiedliche
Teilhabe-Modelle bei der Stimmgewichtung sind möglich.

## Wofür lässt sich Blind-Signierung in einer Wahl verwenden?

Beleg/Signierung eines Wahlrechts für ein Pseudonym, welches die Signierungsstelle
selbst nicht kennt (die kennen nur meine Identität). Eine Entkopplung von 
"Identität" zu "Wahlrecht" ist möglich. Die, die mein Pseudonym an der Wahlurne 
sehen, kennen meine Identität nicht. Eine Identitäts/Personenliste im Wahllokal
wäre überflüssig.

## Was versteht man unter glaubhafter Absteitbarkeit?

Es muss möglich sein, dass es glaubhaft keine Beweise für eine Tat/Wahl gibt.
Beispiel: Nach einer elektronischen Wahl sollte es keiner Behörde möglich sein
zu beweisen, was ich gewählt habe (auch nach dem forensischen Auswerten meines
Geräts, mit dem ich das eVoting durchgeführt habe).

Ein anderer Punkt ist die Everlasting Privacy: Was, wenn die heute verwendete
Verschlüsselung in 100 Jahren knackbar ist? Werden meine Ururenkel
dafür politisch belangt, wenn ich nicht "Grün" gewählt habe? Kennen wir
die ethische/politische Entwicklung in der Zukunft? Die geheime Wahl muss auch
über meinen Tod hinaus geheim bleiben.






# 10 Overlays

Hier auch: Pay to skript Hash, wo ein Hash eines Skripts
angegeben ist, um den Output nutzen zu können. Will man den Output
einer P2SH Tx nutzen, muss man ein Skript welches zum Hash passt, angeben.
Die Ausführung dieses Skripts muss erfolgreich sein. Toll: Niemand
kann erkennen, wie der Output zu verwenden ist, weil niemand dessen
Überprüfung kennt.


## Wie wird Secure-Timestamping in Bitcoin verwendet

- Statt echtem Zeitstempel ist hier eine relative Zeit: Abfolge von Ereignissen
- Unveränderlicher Append Only Log
- Commitments (z.B. Patentanmeldung)
- Sicherer Messanger: Durch Multi-Signierung einer Nachricht kann hinterher
  niemand behaupten, man habe etwas nicht gewusst

sende Hash der Daten in Out statt Hash eines Public-keys. Die Coins
sind danach nicht mehr nutzbar.


## Was sind Colored-Coins

- Vergleichbar mit einem Text, den man auf einen echten Geldschein schreibt
- Man nutzt die Information der Transaktions-Vergangenheit aus, um gewollt
  zusatzinfos zu einem Coin zu hinterlegen
- Ein Skript Hash bzw. Skript, was erfolgreich durchläuft, vereinfacht
  komplexität einer Transaktion (multisign ist nicht mehr zwingend nötig)
- Verwendung von Coins bekommten ein Zusatzkriterium aufgezwungen
- man braucht die ganze Tx Historie zur Validierung
- Marker: ein kleiner unspendable Output, der immer verbraucht wird

## Was sind OpenAsserts (bitte 3 Beispielanwendungen)

Asserts sind eigen definierte Werte innerhalb der Blockchain

- key-value wie IP-domain name
- Unternehmensanteile
- Schlüssel für Nutzungsrechte z.B. Türöffner
- Mietwagen starten

## Welcher maßgeblicher Unterschied existiert zwischen FIAT und Bitcoin

Das Geld in Bitcoin hat eine Vergangenheit - man kann, wenn man es nicht verhindert,
sehr leicht erkennen, wo und wie oft es schon mal gebraucht wurde. Problem
beim kauf an Supermarktkasse:

- ich muss 1h warten, bis es sicher ist, dass TX in Chain ausreichend verifiziert ist
- aktuell Große schwankungen bzgl. des Wertes/Wechselkurs
- mit nur ca 7 Tx/s viel zu langsam, um Weltweit täglich zu verwenden.




# 11 Mining Pools

## Nenne 2 Auszahlungs-Strategien von Mining Pools und beschreibe sie.

Wer trägt außerdem das Risiko sollten Blöcke nicht erzeugt werden können?

**Pay-per-share**: Pool manager trägt risiko und zahlt für Share (mining nachweise) aus.

**Proportional Pay**: Risiko gleichverteilt. Erst bei blockerzeugung wird ausgezahlt
z.B. proportional zum geloggten Share des mit-miniers. Allerdings könnte sich
hier der Admin aus dem Staub machen, sollte coinbase nicht mehrere Output
an alle Miner haben (geht das?)

## Beschreibe kurz alle 4 Möglichkeiten eines Miners, die er im Pool entscheiden muss.

- Welche Transaktionen sollen in den Block: Fee überschreitet nicht minimalgebühr
- Vorgänger Block: den, der längsten bekannten Kette
- Ketten gleich lang: nehme den Vorgänger Block, der am ältesten ist (pick first?!)
- wann wird neuer Block publiziert? (sofort)


## Warum hat ghash.io sich selbst auf 39.99% hashing power reduziert? 

Bei mehr als 50% hashing power würde bitcoin nicht mehr als sicher gelten. Daran haben
Poolbetreiber kein Interesse.

## Was gewinnt man durch zurückhalten von Blöcken?

Man kann dadurch einen anderen Pool schwächen. Der Block kann auch genutzt werden,
um darauf aufbauend einen weiteren Block zu erzeugen, um die Chain des Pools zu überholen.
Das ist aber schnell erkennbar, da auf einen Schlag mehrere Blöcke bekannt werden.
Zusätzlich wird der hashingpower der anderen poolmitglieder in der
zwischenzeit verschwendet.

## Welche Bedingungen müssen erfüllt sein, um Transaktion abzulehnen? Wie plausiebel ist das?

**die antwort auf die frage ähnelt sehr der antwort auf feather foring**

Grundsätzlich ist man nicht gezwungen, jede Transaktion beim
bilden eines neuen Blocks auf zu nehmen. Auf dauer macht es jedoch keinen
Sinn, da es unwahrscheinlich ist eine so lange Kette von Blöcken zu bilden,
in der man diese Tx nicht aufnimmt.

Zusätzlich wird in späteren Tx dessen Output verwendet... solche Tx
müssten dann genauso abgelehnt werden. Das "läppert" sich.

vorsicht/unfug/ungenau:

Wenn ich eine gewisse Möglichkeit habe, Blöcke zu erzeugen in denen
die Tx eines Accounts nicht auftauchen und ich Blöcke ignoriere mit solchen Tx
einer gleich langen Chain, dann werden andere diese Tx irgendwann auch ignorieren.
Andere Pools wollen ihre Blöcke auch gerne in der Chain haben und nicht
aufgrund eines gewissen Prozentsatzes gefahr laufen, ignoriert zu werden.
Daher werden diese irgendwann auch diese Tx ignorieren.

Es ist eigentlich nur plausibel, sollte man einen echten finanziellen oder Privaten
Vorteil dadurch haben. Dies kann in Real-World szenarien erpressung oder eine
sehr langfristige Planung der Sabotage sein.

## Was ist feather forking und warum ist dieser Name angemessen?

Die Idee ist, dass man sobald eine Tx (an oder von) einer Person
im Netzwerk auftaucht, an einem Fork arbeitet, in dem diese Tx nicht
auftaucht. Besitze ich z.B. 20% hashingpower des Netzwerks, so ist
die Wkeit nicht 1 nächsten sondern gleich 2 nächste Blöcke vor den
anderen Peers zu bliden 0.04 (4%). Diese "+1" Forks an der aktuellen
Chain ähneln in einer Grafik einer Feder. 

Der Effekt, wenn man damit ein paar mal erfolg hat: andere ignorieren
Tx mit dieser Person (also sie erstellen gar keine Tx mehr mit dieser Person)
da sie es sich nicht leisten können, dass 4% der Tx nicht erfolgreich
sind. Es setz jedoch voraus, dass man die ganze Zeit feather forking
betreibt.






# 12 Arbitrary Content

## Welche Arten der Insertion gibt es? 

- Coinbase Parameter
- unspendable Output: der Hash wird als Datenfeld genutzt
- Bereich hinter OP_RETURN
  - 80 Bytes
  - verbrennt Coins
  - Tx könnte ignoriert werden
- Komplexe Skripte mit "DEAD CODE", in dem dann inhalt steht
  - Tx könnte ignoriert werden
  - verbrennt normalerweise keine Coins
- Verstecken der Daten in "schlechtem" Private-Key (CommitCoin) (errechenbar nach genug Signierungen)
- vanity addresses
- hashlock mit offenlegung des geheimnis

## Wofür kann "Arbitrary Content" (legal) genutzt werden?

- Coinbase: Werbung für bestimmte Features
- Colored Coin, OpenAsserts
- Commitments (Besitze unverfälschte Info zum Zeitpunkt x)
- Signierung als Bestätigung zu einem Vertragstext zwischen Partnern
- Patente
- Proof of Burn (?)

## Welche Risiken hat "Arbitrary Content" in Bitcoins Blockchain?

Besitz der Chain könnte in manchen Ländern illegal sein. Es ist
rechtlich nicht klar, ob man aktiv beteiligt ist, wenn man
ein unzensiertes Netzwerk "unterstützt", deren Kernanwendung
nicht zur Verbreitung solcher Inhalte ist. Kritisch:

- Volksverhetzung
- unerlaubte Fakten
  - Mondlandung (Nordkorea)
  - Massakter am Platz des himmlischen Friedens (China)
- (Kinder)pornographie
- Privatsphäre, Persönlichkeitsrechte (Adressen, Bilder)
- Urheberrecht (DVD oder Spiele Keys)
- Spionage oder Landesverrat (Wikileaks)





# 13 Altcoins


## Nenne 3 Altcoins und beschreibe ihre Eigenschaften und dessen Verwendung.

- Namecoin
  - Domainnamen (IP-name, key-value)
  - kauf zerstört Coins
  - handel von Domainnamen ist möglich
- Peercoin
  - erste Verwendung des Proof of Stake
  - Coin Besitzer dürfen sich Puzzel leichter machen
  - Ziel: Energieeffizienter Bitcoin-Ersatz
  - Nextcoin: Voting, Messaging, Asserts, Geldersatz
- Etherium
  - toringvollständige Skriptsprache
  - "Gas" Verbrauch verhindert endlos-loops
  - höhere Block und Tx rate als Bitcoin
  - Smart-Contracts, Geldersatz, allgemeine P2P bzw. verteilte Anwendungen
- Litecoin
  - scrypt als memory-hartes Puzzel
  - sollte GPU mining verhindern
  - nicht/kaum ASIC resistent
  - Geldersatz
- Primecoin
  - Konzept: Proof of useful Work
  - das Ergebnis des Puzzels ist mathematisch nützlich
  - Cunningham Ketten werden gesucht
  - Geldersatz


## Angenommen man würde einen neuen Altcoin auf Basis von Bitcoin kredenzen.

Mit welchen Herausforderungen ist man konfrontiert in der start-Phase dieser Währung?

- Wie finde ich Miner für meine Währung?
- Wie finde ich Leute, die meine Währung nutzen wollen?
- Wie finde ich zu Anfang genug Miner, so dass die Währung 51 % Attacken abfedert?
- Wie bekommt mein Block Reward einen Wert?

zu 1 und 2) Möglichkeit wäre, dass ich Bitcoin Blöcke und Txs adoptiere, jedoch
das Puzzle leichter mache. So kann man leicht auf meine Währung wechseln und
Miner könnten ggf. für mich statt für Bitcoin einen Reward einstecken, anstatt
dass der gefundene fail-Block in Bitcoin im Schrott landet.

der Rest: Initial ganz viele Blöcke machen und ICO Anteile versteigern und ggf. Verschenken.
Sponsoren finden. Tausch anbieten gegen Bitcoins.


## Beschreiben Sie merge mining und wie man ein beliebiges Intervall an neuen Blöcken erreichen kann.

- einbau Tx von Bitcoin in meine Chain ermöglichen
- umgekehrt: nutzen der Coinbase von Bitcoin, um dort einen Root-Hash meiner Tx meiner Chain zu hinterlegen

Sollten nur 10% der Bitcoin Miner meine Tx Hash Root in die Coinbase ihrer Blockchain
Blöcke schreiben, damit der Header der Bitcoin Blöcke auch als Blöcke meiner Blockchain
genutzt werden können, dann gibt es nur ca alle 100Min einen neuen Block. Trick:
die Schrott-Blöcke, die eine zu geringe Difficulty für Bitcoin haben könnte ich für
meine Chain zulassen.

## Wie kann man Coins zwischen 2 Währungen transverieren ohne einem Dritten vertrauen zu müssen?

Atomic Cross-Chain Swap Protokoll

Genau wie beim Waren-Versand gibt es ein Problem, wenn beide sich gegenseitig nicht Trauen:
Ich will eigentlich erst zahlen, wenn die Ware da ist und der Hersteller will erst die
Ware senden, wenn er das Geld hat. Klassisch würde hier nun ein Treuhänder (Escrow, Multisign)
genutzt, dem beide trauen. In dem Fall würde das Geld nachweislich auf Eis gelegt.

Beim Atomic Cross-Chain Swap Protokoll ist eine ähnliche Situation, nur ist es hier
keine Ware, sondern es sind Coins einer fremden Währung. Außerdem kommt man ohne einer
3. Treuhand-Person aus. Dies geht über 2 Hashlock und 2 Timelock Transaktionen, wobei
die Timelock Tx einen Revoke bedeuten, sollte der Andere nicht im Zeitrahmen die
Coins in einer weiteren Transaktion ausgeben/sicherstellen. Diese Revoke Transaktion
wird von beiden signiert - Quasi als Vertragsunterschrift, eine Revoke Tx machen
zu können, wenn der Andere etwas verbummelt.

### Ablauf

Alice und Bob sind in Altcoin und Bitcoin existent und wollen. Alice will a viele Altcoins dem
Bob geben und dafür soll Bob der Alice b viele Bitcoins geben. 

- Alice den denkt sich ein Geheimnis x aus und hasht es
- Alice macht eine Tx1, die mit Bobs Signatur die Verwendung von a Altcoins ermöglicht, wenn diese x zum hash(x) veröffentlicht
- Der Output von Tx1 außerdem verwendbar, wenn er in einer Tx genutzt wird, die von Alice und Bob signiert ist
- Tx1 wird noch nicht veröffentlicht (sonst bekommt Alice ggf. ihre Altcoins nicht mehr wieder)
- Alice baut sich nun eine Tx2, mit der sie frühstens in 48h über den Output der Tx1 verfügen kann.
- Alice signiert Tx2 und sendet diese an Bob, damit dieser Tx2 auch signiert.
- Nun hat Alice mit Tx2 eine Möglichkeit, nach 48h den Output von Tx1 zurück zu bekommen
- Alice veröffentlich Tx1 im Altcoin Netzwerk
- Bob sieht in der Tx1 der Altchain den hash(x) und baut nun eine ähnliche (Tx3) für die Bitcoin-Chain
- Bobs Tx3: b Bitcoins durch Alice Signatur einlösbar, sollte sie x zu hash(x) publizieren
- Bobs Tx3: außerdem/oder ist der Output einlösbar/verwendbar, wenn eine Tx4 durch beide signiert ist
- Genau wie Alice macht nun Bob eine signierte Tx4, welche den Output von b Bitcoins an sich frühstens in 24h (<48h) zurück überweisst
- Bob signiert Tx4 und lässt diese auch von Alice signieren
- Bob hat mit Tx4 nun die Möglichkeit, in frühstens 24h seine b Bitcoins zu retten
- Bob kann nun die Tx3 in Bitcoin Netzwerk veröffentlichen

Nun ist Alice am Zug, wenn sie will. Egal, ob sie x im Altcoin Netzwerk oder im Bitcoin Netzwerk
veröffentlicht, Bob bekommt es mit. Allerdings macht die Veröffentlichung im Bitcoin Netzwerk
Sinn, da Alice Bitcoins haben wollte und mit Tx3 eine Output skript dies ermöglicht. Alice
könnte x aber auch Bob zusenden, damit dieser im Altcoin seine Coins einlösen kann. Durch
den Timelock der von beiden Signierten Revoke-Transaktionen ist dafür genug Zeit. Selbst
wenn Tx2 und Tx4 versehentlich in die jeweiligen Netzwerke kommen, so werden diese erst
Aktiv nach der eingebauten Zeitspanne. Sollten Bob oder Alice die Zeitspanne verpennen
und den Output von Tx1 bzw. Tx3 nicht "ins Trockene" bringen, kann der jeweils Andere
diese mit Tx2 bzw. Tx4 zurückholen. Löst man die Coins aber ein, so kann man
kein Revoke mehr machen, der man den Output nicht 2x ausgeben kann.



Auch interessant: [nemtech Cross-chain-swaps](https://nemtech.github.io/concepts/cross-chain-swaps.html)
und [Bitcoin HTLC](https://en.bitcoin.it/wiki/Hash_Time_Locked_Contracts)






# 14 Etherium



## Gas: nenne 2 Probleme, die damit gelöst werden und warum diese gelöst werden.

- Turing-vollständige Skriptsprache kann Endlosschleifen erzeugen und Ausführungsdauer ist nicht berechenbar: geht das Gas aus, stoppt das Skript trotzdem
- Speicher und CPU Nutzung kostet Energie/Geld: Bezahlung bei Skriptausführung
- Man kann evtl. Skripte priorisieren, indem man deren Ausführung attraktiver macht: viel Gas je Schritt/Byte
- Verhindern von SPAM, da alles etwas kostet

## Beschreiben Sie zwei... Anwendungen von Ethereum.

- Dezentraler Dateispeicher mit dem Anspruch so sicher wie eine Finanztransaktion zu sein
- Sub-Währungen und Asserts sind mit Gas sowie smart properties direkt umsetzbar und von Etherium 
  unterstützt (Fees in der Subwährung sind ebenfalls möglich)
- DAO: Dezentrale Autonome Organisation: accounts erhalten Stimmrecht zur Änderung eines Smart-Contract Codes.
  Derzeit via Smart-Property gelöst, in der die Adresse eines aktiven Smart-Contract Codes liegt.
- Namecoin (Key-Value storage)
- Finanzielle Derivate, also Code und Smart Contracts, die sich auf mehrere Quellen von Wechselkursen beziehen können.


## Graph mit Knotenmenge V={a1, a2, b2, a3, b3, a4} und Kantenmenge E={(a2, a1), (b2, a1), (a3, a2), (b3, a2), (a4, a3)}.

- Wie viele uncles kann a4 maximal haben? 2: b2 und b3
- Welches ist die längste Sub-Chain? a3 - a4
- in der Klausur mit anderem Graphen könnte längste Sub-Chain weniger trivial sein!

b2 als direkter Vorfahre von b3 ginge nicht als uncle für b3. b2 würde aber alternativ
zu b3 als uncle für a4 gehen. sollte a3 aber bereits b2 als uncle genommen haben,
so kann a4 diesen nicht mehr als uncle nehmen.

Beispiel mit 2 uncles, bei dem einer sogar 2 generationen zurückliegt: [etherscan.io](https://etherscan.io/block/8065154)

## Wie viele uncles muss a4 minimal haben, unter ann., dass jeder mögliche Knoten auch ein uncle wird?

a3 hätte b2 als uncle nehmen können oder b3 hätte b2 als uncle nehmen können (b2 ist nicht parent von b3).
a4 kann in jedem fall b3 als uncle nehmen. Die Antwort ist also 1.

## Wieso will man bei der Erstellung der Chain ein Uncle aufnehmen?

Die dickste statt die längste Chain soll genommen werden. Auf diese Weise bekommen
Miner bei parallel erzeugten Blöcke ebenfalls ein Block-Reward/Fee. Das macht
das Minig attraktiver, da weniger Hashing-Power eines Miners im Vordergrund
steht, um zu profitieren. 





### Unklar

Es spielt auch die hohe Blockerzeugungsrate und die Verbreitungsgeschw. der
Blöcke im Netzwerk eine rolle (????), warum man "GHOST" macht.

### Mögliche andere Fragen (?)

- Wann ist Block Uncle tauglich:
  - war zuvor noch nie Uncle
  - nur block header muss valide sein
  - muss direkter Child eines Blocks X (nicht subchain) sein, wobei X 2 bis 7 Generationen zurück liegt
  - darf nicht gleichzeitig parent sein
- Warum 2 Arten von Accounts
- Unterschied Message und Transaktion
- was ist GHOST [google sagt, Etherum hat das nicht 100 proz. umgesetzt](https://ethereum.stackexchange.com/questions/38121/why-did-ethereum-abandon-the-ghost-protocol)
- Warum 3 Merkle tries? (Txs, States, Empfänger???)
- was ist mit Patricia gemeint
- warum ist nicht vorhersehbar wie lange skript läuft
- EVM code und welche Hochsprache wird genutzt
- Wieso gibt es auch hier Mining-Pools? (Man braucht die ganze Chain, welche in Etherium viel RAM verbraucht).
- Wie sehe eine mögliche Umstellung auf PoS aus?

Mainchain(immernoch PoW), Beaconchain ist dessen Childchain (merge chain?)
an der 1024 Shards hängen und Validator Manager Contact je 
Shard mit Ether, um 51% Attacke im Shard zu verhindern. Vaidator delegiert,
wer Block machen muss. Mein Ether erhöht wkeit, dass ich gewählt werde.






# 15 Braucht man eine Blockchain

## Was ist der Unterschied zwischen Permissioned und Permissionless Blockchain?

Eine Permissioned Blockchain verteilt an Writer (und ggf. auch an Reader)
Berechtigungen via zentraler Instanz. In der Regel wird ein BFT (Byzantine Fault
Tolerance) Konsens Protokoll unter den Schreibern verwendet. Aufgrund der
geringeren Zahl an Schreibern (und auch geringeren Zahl an untrusted Schreibern)
wird so ein höherer Durchsatz erreicht als in einer permissionless Blockchain.


## Welches Dilemma besteht zwischen Transparency und Privacy?

Zum einen will man die Daten so transparent (Einsicht) wie möglich,
um öffentlich prüfen zu können, ob alles korrekt ist. Hierbei sollte
es sich jedoch primär um Datenstruktur und Prozessdaten gehen und nicht
um die Teilnehmer und deren Informationen (Privatsphäre). Ein gutes
Beispiel bei diesem Zwiespalt, was man gerne an Transparenz will und
was man an Privatsphäre will: eVoting. Dort will man gerne selbst überprüfen
können, ob die Stimme im elektronischen System korrekt abgelegt ist -
gleichzeitig sollte es nicht möglich sein, dass andere vorzeitig die
Stimme auswerten oder durch Diebstahl des Rechners, mit dem man Wählte,
ebenfalls die abgegebene Stimme identifizieren.

## Welches Konsensverfahren wird bei Bitcoin eingesetzt?

Bitcoin verwendet PoW als Konsensverfahren. Ähnlich wie bei Raft wählt sich
ein Knoten selbst als Ledger, wobei ein Kryptorätsel als zufälliges
Zeitfenster dient. Diese Rätsellösung und der Block mit seinen Transaktionen
können von den anderen Knoten auf Richtigkeit geprüft und akzeptiert werden.

## Warum eine zentralisierte Datenbank bessere Leistung in Bezug auf Durchsatz und Latenz als eine Blockchain erzielt?

Berechtigungen sind zentral bekannt und zentral verwaltet, daher ist eine 
Konsensfindung nicht notwendig.

## Was ist der Unterschied zwischen Hyperledger Fabric und anderen Blockchain-Plattformen?

Naja, diese Kriterien gelten zum Teil auch für andere Blockchain-Plattformen bis auf "von der Linux Fondation gehostet":

- losgelöst von reiner Kryptowährung-Anwendungen
- Ein Blockchain-as-a-Service der Linux Fondation
- API für gängige Programmiersprachen
- permissioned Blockchain


## Entscheidungshilfe

- Finden Statusänderungen statt, die abgespeichert werden müssen?
  - ja: siehe nächste Frage
  - nein: **keine Blockchain!**
- Haben wir viele Schreibzugriffe?
  - ja: siehe nächste Frage
  - nein: **keine Blockchain!**
- Entfällt die Möglichkeit, einen Clouddienst (Drittanbieter) zu verwenden?
  - ja: siehe nächste Frage
  - nein: **keine Blockchain!**
- Sind die Leute mit Schreibrecht völlig unbekannt?
  - ja: **Permissionless Blockchain**
  - nein: siehe nächste Frage
- Ich kenne die Leute mit Schreibrechten, ich vertraue diesen aber nicht?
  - ja, ich traue diesen Leuten nicht: siehe nächste Frage
  - nein, die Leute sind Vertrauenswürdig: **keine Blockchain!**
- Die Statusänderungen der nicht vertrauenswürdigen Leute mit Schreibrecht müssen prüfbar sein. Müssen diese von jedem (=öffentlich) prüfbar sein?
  - ja: **Public Permissioned Blockchain**
  - nein: **Private Permissioned Blockchain**

Inoffizielle Frage: Erläutern sie anhand der Entscheidungshilfe, ob
bei den folgenden Anwendungen eine Blockchain sinnvoll ist?

- erteilen einer Wahlberechtigung (nicht die Wahl selbst)
- Autonomes Fahren oder KI Entscheidung Loggen, durch Versicherungen auslesbar
- Krankenakte
- Grundbuch
- Zoll, Logbuch einer Kontainerfüllung
- Fahrtenschreiber
- Funkmeldungen weltweiter Flugverkehr
- Daten der Blitzortung oder Flugzeugtranspondersignal
- Geschichtsbücher
- Evolution in der Biologie: Ist die DNA eine natürliche Blockchain?
- KI Coin: nur wenn alle KIs der Meinung sind, dass im Block eine "Bild mit inhalt XY" zu sehen ist, wird er akzeptiert. Das Bild muss aber neu sein.
 