# NOW := $(shell date +"%c" | tr ' :' '__')
#NOW := $(shell date +"%Y%m%d-%H%M")
OUTFILE_TITLE = Proof of Stake und E-Voting
OUTFILE_SUBTITLE = Mehr Teilhabe und Sicherheit für alle (?)
OUTFILE_AUTHOR = Jochen Peters
OUTFILE_INSTITUTE = Department of Computer Science \\\\ Heinrich-Heine-University Düsseldorf, Germany
#OUTFILE = ./Slides.$(AUTHOR).$(NOW).pdf
OUTFILE = ProofOfStake_eVoting.pdf

all:
	sed -e 's:__OUTFILE_TITLE__:$(OUTFILE_TITLE):g' Readme.tex > temp2_Readme.tex
	sed -e 's:__OUTFILE_AUTHOR__:$(OUTFILE_AUTHOR):g' temp2_Readme.tex > temp_Readme.tex
	sed -e 's:__OUTFILE_SUBTITLE__:$(OUTFILE_SUBTITLE):g' temp_Readme.tex > temp2_Readme.tex
	sed -e 's:__OUTFILE_INSTITUTE__:$(OUTFILE_INSTITUTE):g' temp2_Readme.tex > temp_Readme.tex
	sed -e 's: ,,: \\glqq{}:g' Readme.md > temp.md
	# png ist in Markdown gut, aber eps ist in LaTeX besser (use GIMP!). Beide Bilddateien noetig
	sed -i -e 's:\.png):\.eps):g' temp.md
	# in diesem Fall sind die Bilder im images-Unterordner. Die LaTeX-Dateien aber auch!
	sed -i -e 's:](images/:](images/:g' temp.md
	# terrible c++ hightlightning hack
	sed -i -e 's:~~~java:\\begin{minted}{java}:g' temp.md
	sed -i -e 's:~~~cpp:\\begin{minted}{cpp}:g' temp.md
	# xml not realy working with pandoc and minted !!!
	sed -i -e 's:~~~xml:\\begin{minted}{xml}:g' temp.md
	sed -i -e 's:~~~bash:\\begin{minted}{bash}:g' temp.md
	sed -i -e 's:~~~:\\end{minted}:g' temp.md
	# pandoc :-D
	pandoc --highlight-style=pygments -f markdown+multiline_tables --slide-level 2 -t beamer temp.md -o temp.tex
	latex -shell-escape temp_Readme.tex
	latex -shell-escape temp_Readme.tex
	dvips -Ppdf temp_Readme.dvi -t landscape
	ps2pdf temp_Readme.ps
	mv temp_Readme.pdf $(OUTFILE)

tex:
	latex -shell-escape temp_Readme.tex
	dvips -Ppdf temp_Readme.dvi -t landscape
	ps2pdf temp_Readme.ps
	mv temp_Readme.pdf $(OUTFILE)

clean:
	rm -rf _minted-temp_Readme
	rm -rf temp*
